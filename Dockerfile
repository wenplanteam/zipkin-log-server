FROM openjdk:8
COPY /build/libs/zipkin-log-server-0.0.1-SNAPSHOT.jar zipkin-log-server.jar
EXPOSE 9902
ENTRYPOINT ["java" , "-jar", "zipkin-log-server.jar"]



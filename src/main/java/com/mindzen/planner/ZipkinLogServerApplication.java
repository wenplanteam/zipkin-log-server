package com.mindzen.planner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import zipkin2.server.internal.EnableZipkinServer;

@SpringBootApplication
@EnableZipkinServer
public class ZipkinLogServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZipkinLogServerApplication.class, args);
	}
}
